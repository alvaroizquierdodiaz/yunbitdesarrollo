-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 02-05-2019 a las 20:31:45
-- Versión del servidor: 8.0.16
-- Versión de PHP: 7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `yunbitdesarrollo`
--
CREATE DATABASE IF NOT EXISTS `yunbitdesarrollo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `yunbitdesarrollo`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ENTRADAS`
--

DROP TABLE IF EXISTS `ENTRADAS`;
CREATE TABLE `ENTRADAS` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `ENTRADAS`
--

INSERT INTO `ENTRADAS` (`ID`, `NAME`, `DESCRIPTION`) VALUES
(1, 'Entrada uno', 'Descripción ejemplo entrada uno'),
(2, 'Entrada dos', 'Descripción ejemplo entrada dos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ENTRADAS_TAGS`
--

DROP TABLE IF EXISTS `ENTRADAS_TAGS`;
CREATE TABLE `ENTRADAS_TAGS` (
  `ID_ENTRADA` int(11) NOT NULL,
  `ID_TAG` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `ENTRADAS_TAGS`
--

INSERT INTO `ENTRADAS_TAGS` (`ID_ENTRADA`, `ID_TAG`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TAGS`
--

DROP TABLE IF EXISTS `TAGS`;
CREATE TABLE `TAGS` (
  `ID` int(11) NOT NULL,
  `TAG` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `TAGS`
--

INSERT INTO `TAGS` (`ID`, `TAG`) VALUES
(1, 'Etiqueta 1'),
(2, 'Etiqueta 2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ENTRADAS`
--
ALTER TABLE `ENTRADAS`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `ENTRADAS_TAGS`
--
ALTER TABLE `ENTRADAS_TAGS`
  ADD PRIMARY KEY (`ID_ENTRADA`,`ID_TAG`),
  ADD KEY `ID_TAG` (`ID_TAG`);

--
-- Indices de la tabla `TAGS`
--
ALTER TABLE `TAGS`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ENTRADAS`
--
ALTER TABLE `ENTRADAS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT de la tabla `TAGS`
--
ALTER TABLE `TAGS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ENTRADAS_TAGS`
--
ALTER TABLE `ENTRADAS_TAGS`
  ADD CONSTRAINT `ENTRADAS_TAGS_ibfk_1` FOREIGN KEY (`ID_ENTRADA`) REFERENCES `ENTRADAS` (`ID`),
  ADD CONSTRAINT `ENTRADAS_TAGS_ibfk_2` FOREIGN KEY (`ID_TAG`) REFERENCES `TAGS` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
