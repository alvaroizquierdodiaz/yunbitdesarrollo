<?php
session_start();
include 'conexion/conecction.php';

if(isset($_POST['add'])){
    $database = new Conexion();
    $db = $database->open();
    try{
        // hacer uso de una declaración preparada para evitar la inyección de sql
        $stmt = $db->prepare("INSERT INTO ENTRADAS(Name, Description) VALUES(:Name, :Description)");
        // declaración if-else en la ejecución de nuestra declaración preparada
        $_SESSION['message'] = ($stmt->execute(array(':Name' => $_POST['name'] , ':Description' => $_POST['description']))) ? 'Entrada agregada correctamente' : 'Se ha producido un fallo';
        
    }
    catch(PDOException $e){
        $_SESSION['message'] = $e->getMessage();
    }
    
    //cerrar conexión
    $database->close();
}

else{
    $_SESSION['message'] = 'Rellena los datos primeramente';
}

header('location: index.php');

?>
